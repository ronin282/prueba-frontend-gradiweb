const ProductDescription = ({productDescription}) => {
  return <div dangerouslySetInnerHTML={{ __html: productDescription  }} />
}

export default ProductDescription;