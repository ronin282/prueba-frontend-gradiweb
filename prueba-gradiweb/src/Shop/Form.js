import { useState} from "react";
import ModalAddCart from "./ModalAddCart";

function Form({productName, colorOptions, sizeOptions, productPrice, productThumbnail}) {
  // return <div>NadaForm</div>
  const [values, setValues] = useState({
    cantidad: 1,
    totalprice: productPrice,
    colorSelected: colorOptions[0],
    sizeSelected: sizeOptions[0]
  });
  function handleSubmit(evt) {
    evt.preventDefault();
  }
  function handleChange(evt) {
    const { target } = evt;
    console.log(target.value);
    setValues({
      ...values,
      cantidad:target.value, 
      totalprice:productPrice*target.value
    });
  }
  
  function handleChangeVarColor(evt) {
    const { target } = evt;
    console.log(target.value);
    setValues({
      ...values,
      colorSelected:target.value
    });
  }

  function handleChangeVarSize(evt) {
    const { target } = evt;
    console.log(target.value);
    setValues({
      ...values,
      sizeSelected:target.value
    });
  }

  return (
    <div className="selector">
      <form onSubmit={handleSubmit}>
        <div className="separator"></div>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div className="lbl-variant">Color</div>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <select className="form-select" onChange={handleChangeVarColor}>
              {colorOptions.map((item, index) =>(
                <option key={item}>{item}</option>
              ))}
            </select>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div className="lbl-variant">Size</div>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <select className="form-select" onChange={handleChangeVarSize}>
              {sizeOptions.map((item, index) =>(
                <option key={item}>{item}</option>
              ))}
            </select>
          </div>
        </div>
        <div className="separator"></div>
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
            {/* <div className="Label">Cantidad</div> */}
            <input 
            className="form-control input-quantity"
            type="number"
            name="cantidad"
            value={values.cantidad} 
            min="1" 
            max="10"
            onChange={handleChange}/>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="content-total-price">
              <span className="Label lbl-total-price">Total price</span>
              <span className="value-total-price">{values.totalprice}</span>
            </div>
          </div>
        </div>
        <div className="content-button">
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
              <button className="btn-product-add-favorite" type="submit" data-bs-toggle="modal" data-bs-target="#exampleModal">Add to favorite</button>
            </div>
            <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
              <button className="btn-product-add-cart" type="submit" data-bs-toggle="modal" data-bs-target="#exampleModal">Add to cart</button>
            </div>
          </div>
        </div>
        
        <ModalAddCart 
          productName={productName} 
          cantidad={values.cantidad} 
          totalprice={values.totalprice} 
          colorSelected={values.colorSelected} 
          sizeSelected={values.sizeSelected}
          productThumbnail={productThumbnail} />
      </form>
    </div>
  );
}

export default Form;