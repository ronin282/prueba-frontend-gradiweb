const ModalAddCart = ({productName, cantidad, totalprice,colorSelected, sizeSelected, productThumbnail}) => {
  return (
    <div className="modal fade modal-add-cart" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Added to cart</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div className="modal-body">
            <div className="row">
              <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <img className="product-thumbnail" src={productThumbnail} />
              </div>
              <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <h6>{productName}</h6>
                <span className="lbl-variant">Color: </span><span className="value-variant">{colorSelected}</span> 
                <div className="separator-variant"></div>
                <span className="lbl-variant">Size:</span><span className="value-variant">{sizeSelected}</span>
                <div className="quantity">Quantity: {cantidad}</div>
                <div className="totalprice">Total price: $ {totalprice}</div>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-view-cart" data-bs-dismiss="modal">View cart</button>
            <button type="button" className="btn btn-close-modal" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalAddCart;