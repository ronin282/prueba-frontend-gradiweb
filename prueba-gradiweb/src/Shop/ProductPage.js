import { useEffect, useState} from "react";
import Carousel from "../Layout/Carousel";
import Form from "./Form";
import ProductDescription from "./ProductDescription";

const ProductPage  = () => {
 
  const urlInitial = "https://graditest-store.myshopify.com/products/free-trainer-3-mmw.js";

  const [product, setProduct] = new useState([]);

  const [loading, setLoading] = new useState(false);
  
  useEffect(() => {
    setLoading(true);
    console.log("CARGANDO");
    async function fetchProduct(url) {
      const response = await fetch(url);
      const json = await response.json();
      setProduct(json);
      setLoading(false);
      console.log("TERMINO");
    }
    fetchProduct(urlInitial);
  }, []);
  
  if (loading) {
    return <p>Data is loading...</p>;
  }

  try {
    if(product != []){
      const colorOptions = product.variants
        .map((p) => p.option1)
        .filter((i, x, a) => a.indexOf(i) === x);
      
      const sizeOptions = product.variants
        .map((p) => p.option2)
        .filter((i, x, a) => a.indexOf(i) === x);

      return( 
        <div className="row productpage">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div className="breadcrumb">
              Catalog / Sneckers / <span>{product.title}</span>
            </div>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <Carousel media={product.media}></Carousel>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div className="productbrand">by Nike x ALYX</div>
            <h2>{product.title}</h2>
            <div className="precio">{product.price}</div>
            <div className="preciocompare">{product.compare_at_price}</div>
            <Form productName={product.title} colorOptions={colorOptions} sizeOptions={sizeOptions} productPrice={product.price} productThumbnail={product.media[0].src}/>
            <ProductDescription productDescription={product.description}/>
          </div>
        </div>
      );
    }
  } catch (error) {
    return <div>Error</div>  
  }

}

export default ProductPage;