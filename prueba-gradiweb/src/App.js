import './App.css';
import Navigation from './Layout/Navigation';
import ProductPage from './Shop/ProductPage';

function App() {
  return (
    <div>
      <Navigation></Navigation>
        <div className="container">
        <ProductPage/>
      </div>
    </div>
  );
}

export default App;
